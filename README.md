# Rasa_chatbot_demo



## Getting started

## Console commands

These are useful console commands, typically for local tests.

## install requirements

pip3 install -r requirements-actions-dev.txt
pip3 install -r requirements-actions.txt

## run duckling locally

sudo dockerd
sudo docker run -p 8000:8000 rasa/duckling

### Validate local training data

``` Python
# Sometimes needed to validate training data and domain
rasa data validate --domain domain --data data --verbose --debug --max-history 5
```

### Train local model using

``` Python
# Always needed for local testing
rasa train --domain domain --data data
```

### Run local rasa instance

``` Python
# Run a Rasa server locally (mostly not needed)
# You may need it to test websocket connections and messaging.
rasa run

# Run a Rasa shell interface locally (to interact with bot via terminal)
rasa shell --debug

# Run the action server locally (always needed for local testing)
rasa run actions --debug
```

### Run local test cases

``` Python
rasa test
```

## Action server configs

The action server connects to several internal and external endpoints. Internal ones include GraphQL, Rasa X, GitLab, and various databases. External APIs include *to be added*. The "configs" folder stores the configuration for these endpoints in three files:

* action_server_config.yaml
* credentials.yaml
* endpoints.yaml

Each file uses the YAML format. Credentials.yaml contains passwords and tokens, and endpoints.yaml the URLs, ports, etc. The action_server_config.yaml stores information about, e.g. the language used in the action server. It is important for the localization function, for instance.

Every server or local deployment of this Rasa chatbot must prepare these configuration files. Mount these files to the action-server container if you use Docker on a server.