# Extend the official Rasa SDK image
FROM rasa/rasa-sdk:3.2.1

# Use subdirectory as working directory
WORKDIR /app

# Copy any additional custom requirements, if necessary (uncomment next line)
COPY requirements-actions.txt ./

# Change back to root user to install dependencies
USER root

# Install extra requirements for actions code, if necessary (uncomment next line)
RUN pip install -r requirements-actions.txt
# Download spacy language data
RUN python -m spacy download en_core_web_md
RUN python -m spacy download it_core_news_md

# Copy actions folder to working directory
COPY ./actions /app/actions

# Adds a label to assign ownership of the new container to user.
# https://docs.portainer.io/advanced/access-control 
LABEL io.portainer.accesscontrol.users="stumi1"

LABEL git.commit.hash=""
LABEL git.commit.branch=""
LABEL rasa.sdk.version="3.2.1"

# By best practices, don't run the code with root user
USER 1001
