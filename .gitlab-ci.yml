variables:
  # The docker-in-docker runner uses the following four variables.
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  DOCKER_HOST: tcp://docker:2375
  MOUNT_POINT: /builds/$CI_PROJECT_PATH/mnt
  RASA_IMAGE: rasa/rasa:$RASA_VERSION-full
  PYTHON_IMAGE: python:3.8.15

# Needed for jobs that build Docker images (Docker in Docker).
services:
    - docker:dind

stages:
  - code-quality
  - test
  - build
  - machine-learning
  - deploy

# Only on change if files and sub-directories in the actions directory. 
# strategy "depend" views the child pipeline status in the upstream pipe
code-quality:
  stage: code-quality
  trigger:
    include: ci/ci-code-quality.yml
    strategy: depend
  only:
    changes:
      - actions/**/*
      - ci/**/*
      - .gitlab-ci.yml

unit-tests:
  stage: test
  image:
      name: $PYTHON_IMAGE
  before_script:
    - python -m pip install --upgrade pip
    - python -m pip install rasa==$RASA_VERSION
    - echo "Rasa installed."
    - python -m pip install -r requirements-actions-dev.txt
    - echo "Requirements for actions installed from file."
  script:
    - python -m unittest
  only:
    changes:
      - actions/**/*
      - ci/ci_code_quality.yml
      - .gitlab-ci.yml
  allow_failure: true

# Only used for the staging branch.
# For rules + file changes see:
# https://docs.gitlab.com/ee/ci/yaml/index.html#ruleschanges 
build-action-server-image:
  stage: build
  image: docker:stable
  variables:
    IMAGE: $DOCKER_REGISTRY/$PROJECT/$RASA_ACTION_SERVER_IMAGE_NAME
  rules:
    - if: '$CI_COMMIT_BRANCH == "staging" || $CI_COMMIT_BRANCH == "coala_it" || $CI_COMMIT_BRANCH == "coala_it"' 
      changes:
        - actions/**/*
        - Dockerfile
        - credentials.yml
        - endpoints.yml
        - requirements-actions-dev.txt
        - requirements-actions.txt
        - .gitlab-ci.yml
  before_script:
    - docker info
    - docker login --username $DOCKER_REGISTRY_USER --password $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY
  script:
    - sleep 5
    # Add label information dynamically.
    - sed -i "s/git.commit.hash=""/git.commit.hash=$CI_COMMIT_SHA/g" Dockerfile
    - sed -i "s/git.commit.branch=""/git.commit.branch=$CI_COMMIT_BRANCH/g" Dockerfile
    - echo "Added labels to image."

    - docker build -t $IMAGE:$CI_COMMIT_SHA -t $IMAGE:latest -f Dockerfile .
    - echo "Build completed."
    - docker push $IMAGE:$CI_COMMIT_SHA
    - docker push $IMAGE:latest
    - echo "Build pushed."
  after_script:
    - docker logout
  tags:
    - docker

# Only on change if files and subdirectories in the data/tests directory + selected files.
machine-learning:
  stage: machine-learning
  trigger:
    include: ci/ci-machine-learning.yml
    strategy: depend
  only:
    changes:
      - data/**/*
      - tests/**/*
      - domain/**/*
      - domain.yml
      - config.yml
      - ci/ci-machine-learning.yml
      - .gitlab-ci.yml

# Upload the trained model to the specified Rasa X server. Differentiates branches used in multi-language projects.
# Get an access token and tag the model as active.
#   MODEL_SERVER_IP -> the specific container name for Rasa X and the port: lab-demo_rasa-x_1:5002
# Make sure the GitLab runner joins the network of the model server.
# ! It is important to run this after the ML stage to ensure action-server changes
# ! and related Rasa models match.
update-docker-stack:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "staging" || $CI_COMMIT_BRANCH == "coala_it"' 
  trigger:
    include: ci/ci-update-stack.yml
    strategy: depend


# These are manual rollout steps where nginx redirects a % of users to the newly deployed bot.
# rollout-10:
#   stage: deploy
#   image: python
#   script:
#     - echo "test"
#   when: manual 
#   only:
#       - master

# rollout-20:
#   stage: deploy
#   image: python
#   script:
#     - echo "test"
#   when: manual 
#   only:
#       - master

# rollout-30:
#   stage: deploy
#   image: python
#   script:
#     - echo "test"
#   when: manual 
#   only:
#     - master

# rollout-50:
#   stage: deploy
#   image: python
#   script:
#     - echo "test"
#   when: manual 
#   only:
#     - master

# rollout-80:
#   stage: deploy
#   image: python
#   script:
#     - echo "test"
#   when: manual 
#   only:
#     - master

# deploy-to-model-server-100:
#     stage: deploy
#     image: python
#     before_script:
#         - pip install requests
#     script:
#         - outputModel=$(find models/ -name '*.tar.gz')
#         - echo $outputModel
#         - basename "$outputModel"
#         - file="$(basename -- $outputModel)"
#         - cd models
#         - curl -k -F "model=@$file" "$MODEL_SERVER_IP/api/projects/default/models?api_token=$MODEL_SERVER_API_TOKEN"
#         - ls && cd ..
#         - cd cli/
#         - model=$(echo "$file" | cut -f 1 -d '.')
#         - python activate_model.py $model
#         - echo "All Steps Executed"
#     when: manual
#     only:
#         - master
